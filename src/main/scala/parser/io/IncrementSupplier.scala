package parser.io
import parser.{ DependencyInstance, Options, ParserConf }

object IncrementSupplier {
  def getSupplier(options: Options, conf: ParserConf, fname: String) = {
    val supplier = if (conf.inputIsIncrements())
      new IncrementSupplierFromIncrements(options, fname)
    else
      new IncrementSupplierFromCompleteSentences(options, fname)
    new FilteringSupplier(supplier, conf.filterLemma(), conf.filterMorph(), conf.filterPos())
  }
}

abstract class IncrementSupplier(val options: Options, val fname: String) extends Iterator[(DependencyInstance, Int, Boolean)] {
  val reader = if (options != null) DependencyReader.createDependencyReader(options) else null
  if (options != null)
    reader.startReading(fname)

  def hasNext: Boolean

  def next(): (DependencyInstance, Int, Boolean)
}
