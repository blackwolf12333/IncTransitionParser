package parser.io

import parser.{DependencyInstance, Options}
class FilteringSupplier(
  other: IncrementSupplier,
  filterLemma: Boolean = false,
  filterMorph: Boolean = false,
  filterPos: Boolean = false) extends IncrementSupplier(null, null) {

  override def hasNext: Boolean = other.hasNext

  override def next(): (DependencyInstance, Int, Boolean) = {
    val result = other.next()
    val inst = result._1
    if (filterLemma)
      inst.lemmas = Array.fill(inst.length){"_"}
    if (filterMorph)
      inst.feats = Array.fill(inst.length){Array[String]()}
    if (filterPos)
      inst.postags = Array.fill(inst.length){"_"}
    result
  }
}
