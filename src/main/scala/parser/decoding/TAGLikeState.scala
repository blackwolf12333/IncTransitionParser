package parser.decoding

import parser.{AbstractTransitionParser, DependencyInstance, DependencyParser}

/**
 * Created by koehn on 4/29/15.
 */


abstract class TAGLikeState extends BaseState[TAGLikeState] {

  // var previousState: TAGLikeState = null

  val lastWord: Int
  val heads: Array[Int]
  val words: Array[String]
  override def getWords: Array[String] = words
  val lemmas: Array[String]
  val pos: Array[String]
  val cpos: Array[String]
  /** TODO! deprel not fully implemented yet */
  val deprel: Array[String]
  /** whether a root is already present **/
  val num_vns: Int
  // var score: Double = 0.0
  val previousState: TAGLikeState = null

  def getDescription: String;

  override def stateSequenceString(): String = {
    if (previousState == null) {
      getDescription
    } else {
      previousState.stateSequenceString() + "------------------------------" + getDescription + "\n"
    }
  }

  override def getPreviousState() = previousState;

  private var _fd_string: String = null;

  override def fdInformation(): String = {
    if (_fd_string != null)
      return _fd_string
    val ret = new StringBuilder
    words.foreach(ret.append(_).append("_:_"))
    lemmas.foreach(ret.append(_).append("_:_"))
    pos.foreach(ret.append(_).append("_:_"))
    ret.toString()
  }

  @inline final override def getHeads: Array[Int] = heads

  override def getDeprel: Array[String] = deprel

  // override def setScore(score: Double): Unit = this.score = score

  // override def getScore: Double = score

  override def toInstanceUninstantiated: DependencyInstance = ???

  var _instance: DependencyInstance = null
  override def toInstance(parser: DependencyParser): DependencyInstance = {
    this.synchronized {
      if (_instance != null) {
        _instance
      } else {
        var shorterState = previousState
        if (this == emptyState) {
          shorterState = emptyState
        } else {
          while (shorterState.lastWord == lastWord) {
            shorterState = shorterState.previousState
          }
        }
        val feats = Array.ofDim[String](words.size, 0)
        val newInstance = new DependencyInstance(words, lemmas, cpos, pos,
          feats, heads, deprel, lastWord)
        if (shorterState == emptyState) {
          newInstance.setInstIds(parser)
        } else {
          newInstance.setInstIds(parser, shorterState.toInstance(parser), shorterState.lastWord)
        }
        _instance = newInstance
        _instance
      }
    }
  }


  override def getInstance: DependencyInstance = _instance


  // specialiced insertArray instead of generic because it is used
  // often and only this way can be optimized

  @inline
  def insertArray(from: Array[Int], elem: Int, index: Int) : Array[Int] = {
    val res = new Array[Int](from.length + 1)
    System.arraycopy(from,0,res, 0, index)
    res(index) = elem
    System.arraycopy(from, index, res, index + 1, from.length - index)
    res
  }

  @inline
  def insertArray(from: Array[String], elem: String, index: Int) : Array[String] = {
    val res = new Array[String](from.length + 1)
    System.arraycopy(from,0,res, 0, index)
    res(index) = elem
    System.arraycopy(from, index, res, index + 1, from.length - index)
    res
  }

  /**
    * increases the heads of all attachments to virtual nodes by one.
    * @param heads the heads to modify in place
    * @param lastWord the new last word. attachments >= lastWord are increased.
    */
  @inline
  protected def adjustVNAttachments(heads: Array[Int], lastWord: Int): Unit = {
    // correct attachments to Virtual nodes, they are now moved right by one.
    var headidx = 1
    while (headidx < heads.length) {
      if (heads(headidx) >= lastWord)
        heads(headidx) += 1
      headidx += 1
    }
  }


  /**
    * sorts the VNs in a state by pos, head, and index of first child
    */
  protected def sortVNs() : Unit = {
    // check wheter more than one virtual node exists.
    val numVirtualNodes = heads.length - lastWord - 1
    if (numVirtualNodes <= 1) {
      return
    }
    // create new indices for virtual nodes: sort by pos and head
    // newIndices is a lookup from old position to new
    val newIndices: Seq[Int] = (0 until numVirtualNodes).map(x => { val idx = lastWord+1+x
              (pos(idx),
              heads(idx),
              heads.indexOf(idx),
              x)}).sorted.map(x=> x._4)

    var isSorted = true
    var i = 0
    while (i<newIndices.size) {
      // scala has no break and this list is reasonably short so just run through the whole list
      isSorted = isSorted && (newIndices(i) == i)
      i += 1
    }
    if (isSorted)
      return

    val tmpwords = new Array[String](numVirtualNodes)
    val tmplemmas = new Array[String](numVirtualNodes)
    val tmppos = new Array[String](numVirtualNodes)
    val tmpcpos = new Array[String](numVirtualNodes)
    val tmpdeprel = new Array[String](numVirtualNodes)
    val tmpheads = new Array[Int](numVirtualNodes)


    // switch all the virtual nodes around into tmp arrays...
    (0 until numVirtualNodes).foreach(i => {
      tmpwords(newIndices(i)) = words(i + lastWord + 1)
      tmplemmas(newIndices(i)) = lemmas(i + lastWord + 1)
      tmppos(newIndices(i)) = pos(i + lastWord + 1)
      tmpcpos(newIndices(i)) = cpos(i + lastWord + 1)
      tmpdeprel(newIndices(i)) = deprel(i + lastWord + 1)
      tmpheads(newIndices(i)) = heads(i + lastWord + 1)
    })

    // and put them back
    (0 until numVirtualNodes).foreach(i => {
      words(i + lastWord + 1) = tmpwords(i)
      lemmas(i + lastWord + 1) = tmplemmas(i)
      pos(i + lastWord + 1) = tmppos(i)
      cpos(i + lastWord + 1) = tmpcpos(i)
      deprel(i + lastWord + 1) = tmpdeprel(i)
      heads(i + lastWord + 1) = tmpheads(i)
    })

    // now switch all heads to reflect the new places
    heads.indices.foreach(i => {
      if (heads(i) > lastWord) {
        heads(i) = lastWord + 1 + newIndices(heads(i) - lastWord - 1)
      }
    })
  }
}

/**
case object emptyState extends TAGLikeState {
  override val lastWord: Int = 0
  override val lemmas = Array("<root-LEMMA>", AbstractTransitionParser.VN_NAME, AbstractTransitionParser.VN_NAME)
  override val heads = Array(-1,2,0)
  override val words =  Array("<root>", AbstractTransitionParser.VN_NAME, AbstractTransitionParser.VN_NAME)
  override val deprel =  Array("<no-type>", "", "")
  override val num_vns: Int = 2
  override val pos =  Array("<root-POS>", "NVIRT", "VVIRT")
  override val cpos =  Array("<root-CPOS>", "NVIRT", "VVIRT")
  override def getPreviousState() = null;
  override def getDescription = "initial state";
  override def toString = "emptyState"
  override def toInstance(parser: DependencyParser): DependencyInstance = {
    val feats = Array.ofDim[String](words.size, 0)
    val newinstance = new DependencyInstance(words, lemmas, pos, pos,
      feats, heads, deprel, lastWord)
    newinstance.setInstIds(parser)
    _instance = newinstance
    _instance
  }
}
*/

case object emptyState extends TAGLikeState {
  override val lastWord: Int = 0
  override val lemmas = Array("<root-LEMMA>", AbstractTransitionParser.VN_NAME)
  override val heads = Array(-1,0)
  override val words =  Array("<root>",  AbstractTransitionParser.VN_NAME)
  override val deprel =  Array("<no-type>", "")
  override val num_vns: Int = 1
  override val pos =  Array("<root-POS>", "NVIRT")
  override val cpos =  Array("<root-CPOS>", "NVIRT")
  override def getPreviousState(): TAGLikeState = null
  override def getDescription = "initial state"
  override def toString = "emptyState"
  override def toInstance(parser: DependencyParser): DependencyInstance = {
    val feats = Array.ofDim[String](words.size, 0)
    val newinstance = new DependencyInstance(words, lemmas, cpos, pos,
      feats, heads, deprel, lastWord)
    newinstance.setInstIds(parser)
    _instance = newinstance
    _instance
  }
}

case class AttachNew(prevState: TAGLikeState, word: String, lemma: String, postag: String, cpostag: String, attachTo: Int) extends TAGLikeState {
  // TODO repair attachments wrt VNs!
  override val previousState = prevState
  override val lastWord = prevState.lastWord + 1
  override val words = insertArray(prevState.words, word, lastWord)
  override val lemmas = insertArray(prevState.lemmas, lemma, lastWord)
  override val heads = insertArray(prevState.heads, attachTo, lastWord)
  adjustVNAttachments(heads, lastWord)
  override val deprel = insertArray(prevState.deprel, "", lastWord)
  override val num_vns: Int = prevState.num_vns
  override val pos =  insertArray(prevState.pos, postag, lastWord)
  override val cpos =  insertArray(prevState.cpos, cpostag, lastWord)
  override def getDescription = s"Attach ($word, $lemma, $postag $cpostag) to $attachTo"
  override def toString = s"""AttachNew(${previousState}, "${word}", "${lemma}", "${postag}", "${cpostag}", ${attachTo})"""
  sortVNs()
}

case class ReplaceVN(prevState: TAGLikeState, word: String, lemma: String, postag: String, cpostag: String, replace: Int) extends TAGLikeState {
  assert(replace > prevState.lastWord, "cannot replace normal word!")

  override val lastWord = prevState.lastWord + 1

  val switchNeeded = replace > lastWord

  // Values we don't change can be shared, all others *must* be cloned!
  override val previousState = prevState
  override val words = prevState.words.clone()
  override val lemmas = prevState.lemmas.clone()
  override val heads = prevState.heads.clone()
  override val deprel = prevState.deprel.clone()
  override val num_vns: Int = prevState.num_vns - 1
  override val pos =  prevState.pos.clone()
  override val cpos =  prevState.cpos.clone()

  words(replace) = word
  lemmas(replace) = lemma
  pos(replace) = postag
  cpos(replace) = cpostag

  // switch inserted word with first VN to make sure that the "normal" words are continuous
  if (switchNeeded) {
    val firstVN = lastWord // declare here because outside the block it would be a class member
    val wordstmp = words(firstVN)
    words(firstVN) = words(replace)
    words(replace) = wordstmp

    val lemmastmp = lemmas(firstVN)
    lemmas(firstVN) = lemmas(replace)
    lemmas(replace) = lemmastmp

    val headstmp = heads(firstVN)
    heads(firstVN) = heads(replace)
    heads(replace) = headstmp

    val depreltmp = deprel(firstVN)
    deprel(firstVN) = deprel(replace)
    deprel(replace) = depreltmp

    val postmp = pos(firstVN)
    pos(firstVN) = pos(replace)
    pos(replace) = postmp

    val cpostmp = cpos(firstVN)
    cpos(firstVN) = cpos(replace)
    cpos(replace) = cpostmp

    // switch heads of other words
    var i = 1
    while(i < heads.length) {
      if (heads(i) == firstVN) {
        heads(i) = replace
      } else if (heads(i) == replace) {
        heads(i) = firstVN
      }
      i += 1
    }
  }
  sortVNs()

  override def getDescription = s"ReplaceVN ($word, $lemma, $postag, $cpostag) at $replace"
  override def toString = s"""ReplaceVN(${previousState}, "${word}", "${lemma}", "${postag}", "${cpostag}", ${replace})"""
}


case class PredictVN(prevState: TAGLikeState, vnType: String, attachTo: Int) extends TAGLikeState {
  override val previousState = prevState
  override val lastWord = prevState.lastWord
  override val words = prevState.words :+ AbstractTransitionParser.VN_NAME
  override val lemmas = prevState.lemmas :+ AbstractTransitionParser.VN_NAME
  override val heads = prevState.heads :+ attachTo
  override val deprel = prevState.deprel :+ ""
  override val num_vns: Int = prevState.num_vns + 1
  override val pos =  prevState.pos :+ vnType
  override val cpos =  prevState.cpos :+ vnType
  override def getDescription = s"PredictVN $vnType attached to $attachTo"
  //override def toString = pprint.tokenize(this).mkString("")
  override def toString = s"""PredictVN($previousState, "$vnType", $attachTo)"""
  sortVNs()
}


case class PredictHead(prevState: TAGLikeState, word: String, lemma: String, postag: String, cpostag: String, vnType: String, attachTo: Int) extends TAGLikeState {
  override val previousState = prevState
  // TODO predicted head to the end or always sort
  override val lastWord = prevState.lastWord + 1

  // TODO maybe optimize by not creating intermediate arrays
  override val words = insertArray(prevState.words, word, lastWord) :+ AbstractTransitionParser.VN_NAME
  override val lemmas = insertArray(prevState.lemmas, lemma, lastWord) :+ AbstractTransitionParser.VN_NAME
  // insert head-1 because adjustVNAttachments will increment all attachments to VNs
  // This includes our new word -- which would otherwise be attached out of bounds.
  override val heads = insertArray(prevState.heads, words.length - 2, lastWord) :+ attachTo
  adjustVNAttachments(heads, lastWord)
  override val deprel = insertArray(prevState.deprel, "", lastWord) :+ ""
  override val num_vns: Int = prevState.num_vns + 1
  override val pos = insertArray(prevState.pos, postag, lastWord) :+ vnType
  override val cpos = insertArray(prevState.cpos, cpostag, lastWord) :+ vnType
  override def getDescription = s"PredictHead ($word, $lemma, $postag, $cpostag) with phead $vnType attached to $attachTo"
  //override def toString = pprint.tokenize(this).mkString("")
  override def toString = s"""PredictHead($previousState, "$word", "$lemma", "$postag", "$cpostag", "$vnType", $attachTo)"""
  sortVNs()
}

case class PredictTwoHeads(prevState: TAGLikeState, word: String, lemma: String, postag: String, cpostag: String,
                           vn1Type: String, vn2Type: String, attachTo: Int) extends TAGLikeState {
  override val previousState = prevState
  // TODO predicted head to the end or always sort
  override val lastWord = prevState.lastWord + 1

  // TODO maybe optimize by not creating intermediate arrays
  override val words = insertArray(prevState.words, word, lastWord) :+
    AbstractTransitionParser.VN_NAME :+
    AbstractTransitionParser.VN_NAME
  override val lemmas = insertArray(prevState.lemmas, lemma, lastWord) :+
    AbstractTransitionParser.VN_NAME :+
    AbstractTransitionParser.VN_NAME
  // head of new word will be (words.length-1)-1 (i.e. next-to-last virtual node)
  // insert head-1 because adjustVNAttachments will increment all attachments to VNs
  // afterwards.  Therefore: use (words.length-1)-2 which will become the correct thing.
  // also substract 1 from the first predicted virtual node for later adjustVNAttachments correction
  override val heads = insertArray(prevState.heads, (words.length - 1) - 2, lastWord) :+ (words.length - 1) - 1 :+ attachTo
  adjustVNAttachments(heads, lastWord)
  override val deprel = insertArray(prevState.deprel, "", lastWord) :+ "" :+ ""
  override val num_vns: Int = prevState.num_vns + 2
  override val pos = insertArray(prevState.pos, postag, lastWord) :+ vn1Type :+ vn2Type
  override val cpos = insertArray(prevState.cpos, cpostag, lastWord) :+ vn1Type :+ vn2Type
  override def getDescription = s"PredictTwoHeads ($word, $lemma, $postag, $cpostag) with heads $vn1Type and $vn2Type attached to $attachTo"
  //override def toString = pprint.tokenize(this).mkString("")
  override def toString = s"""PredictTwoHeads($previousState, "$word", "$lemma", "$postag", "$cpostag", "$vn1Type", "$vn2Type", $attachTo)"""
  sortVNs()
}
