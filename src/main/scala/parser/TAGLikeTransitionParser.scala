package parser

import java.util.concurrent._
import java.util.concurrent.atomic.AtomicInteger

import com.google.common.collect.MinMaxPriorityQueue
import com.google.common.util.concurrent.AtomicDouble
import parser.decoding.{emptyState, TAGLikeState}

import scala.collection.mutable

import com.typesafe.scalalogging.Logger
import gnu.trove.set.hash.THashSet
import org.slf4j.LoggerFactory

// import parser.InstanceData


/**
 * Created by koehn on 6/17/15.
 */


class TAGLikeTransitionParser(conf: ParserConf) extends AbstractTransitionParser[TAGLikeState](conf, emptyState) {

  val logger = Logger(LoggerFactory.getLogger("TAGLikeTransitionParser"))

  // this is hack to allow the following behaviour:
  // if the parser is trained on an incremental gold standard, we use NVIRT and VVIRT to
  // distinguish between noun predictions and verb predictions.
  // otherwise we only use one kind of prediction, VVIRT.  Therefore, the emptyState
  // may not have an NVIRT node.
  if (!conf.inputIsIncrements()) {
    emptyState.pos(1) = "VVIRT"
    emptyState.cpos(1) = "VVIRT"
  }


  /**
    * General approach:
    * - AttachNew to all licensed heads
    * - ReplaceVN if all edges (head & dep of VN) are licensed
    * - predictHead if licensed (TODO check if possibleEdges includes VN)
    * - for each result PredictVN
    */
  override def generateNewStateStream(previousStates: Iterable[TAGLikeState],
                                      newWord: String, newPos: String, newCPos: String, newLemma: String): Stream[TAGLikeState] = {

    logger.debug("in generateNewStateStream")
    logger.debug(previousStates.toString())

    var headPOS =  possibleEdgesBetweenPOS.get(newPos)

    // if this POS is unknown, allow attachment everywhere
    if (headPOS == null) {
      headPOS = new THashSet[String]()
      possibleEdgesBetweenPOS.values().forEach(x => headPOS.addAll(x))
    }

    val result = previousStates.toStream.flatMap(state => {
      // this state should have been correctly processed before and therefore have an instance.
      assert(state == emptyState || state.getInstance != null)
      logger.debug("generating from " + state.toString)
      // We can only attach a word to 0 if x === emptyState since
      // otherwise we already have a (normal or virtual) root
      val attachList: mutable.ArrayBuffer[TAGLikeState] = mutable.ArrayBuffer()
      logger.trace("attachlist")
      // We want to enforce a tree structure, but some annotation guidellines attach punctuation to 0.
      // If this is the case, the number of head pos found should be one. Allow attachments to 0
      // in that case.
      val firstHead = if (headPOS.size == 1) 0 else 1
      logger.trace("firstHead")
      logger.trace((firstHead to state.pos.length).toString())
      logger.trace(headPOS.toString())
      logger.trace(state.pos.toString())
      val validHeads = (firstHead until state.pos.length).filter(headid => headPOS.contains(state.pos(headid)))
      logger.trace(validHeads.toString())
      logger.trace("running AttachNew")
      validHeads.foreach(x => {attachList.append(parser.decoding.AttachNew(state, newWord, newLemma, newPos, newCPos, x))})

      val vvirtheadpos = possibleEdgesBetweenPOS.get("VVIRT")
      val nvirtheadpos = possibleEdgesBetweenPOS.get("NVIRT")

      val numVirtNodesLeft = conf.maxNumVirtualNodes() - state.num_vns
      logger.trace(s"$numVirtNodesLeft virtual nodes can be added")
      if (numVirtNodesLeft > 0) {
        // there are two places where vvvalidHeads is needed but maybe we don't need it at all
        // declare here & instantiate as needed
        var vvvalidHeads: IndexedSeq[Int] = null
        if (numVirtNodesLeft > 1) {
          // just implement the word->noun->verb->somewhere case
          logger.trace("running PredictTwoHeads")
          val bottomPOS = if (conf.inputIsIncrements()) "NVIRT" else "VVIRT"
          if (headPOS.contains(bottomPOS)) {
            vvvalidHeads = (firstHead until state.pos.length).filter(headid => vvirtheadpos.contains(state.pos(headid)))
            vvvalidHeads.foreach(x => {
              attachList.append(
                parser.decoding.PredictTwoHeads(state, newWord, newLemma, newPos, newCPos, bottomPOS, "VVIRT", x)
              )
            })
          }
        }
        logger.trace("running PredictHead...")
        if (headPOS.contains("VVIRT") /* && ! state.pos.contains("VVIRT")*/) {
          logger.trace(s"attachment of ${newPos} to VVIRT possible...")
          if (vvvalidHeads == null) {
            vvvalidHeads = (firstHead until state.pos.length).filter(headid => vvirtheadpos.contains(state.pos(headid)))
          }
          logger.trace(s"valid heads found: $vvvalidHeads")
          vvvalidHeads.foreach(x => {
            attachList.append(
              parser.decoding.PredictHead(state, newWord, newLemma, newPos, newCPos, "VVIRT", x)
            )
          })
        }

        if (headPOS.contains("NVIRT") /* && ! state.pos.contains("NVIRT")*/) {
          val nvvalidHeads = (firstHead until state.pos.length).filter(headid => nvirtheadpos.contains(state.pos(headid)));
          nvvalidHeads.foreach(x => {
            attachList.append(
              parser.decoding.PredictHead(state, newWord, newLemma, newPos, newCPos, "NVIRT", x)
            )
          })
        }
      }
      logger.trace("Running ReplaceVN")
      // Instead of attaching, we can also replace a word.
      var vnIndex = state.lastWord + 1
      while (vnIndex < state.words.length) {
        if (headPOS.contains(state.pos(state.heads(vnIndex))))
          attachList.append(parser.decoding.ReplaceVN(state, newWord, newLemma, newPos, newCPos, vnIndex))
        vnIndex += 1
      }

      if (!conf.noTopDownPredictions()) {
        logger.trace("PredictVN")
        //now, predict an additional VN if possible.
        val vnPredictions: mutable.ArrayBuffer[TAGLikeState] = mutable.ArrayBuffer();
        attachList.foreach(newstate => {
          val numleft = conf.maxNumVirtualNodes() - newstate.num_vns
          val predictVNStates = List(newstate)
          if (numleft > 0) {
            // TODO maybe also transitively add possible heads

            val newestWordIndex = state.lastWord + 1
            val newestWordHead = newstate.heads(newestWordIndex)
            val possibleHeads = state.lastWord + 1 :: (if (newestWordHead > 0) List(newestWordHead) else Nil)

            val vvvalidHeads =
              if (vvirtheadpos == null)
                possibleHeads
              else
                possibleHeads.filter(headid => vvirtheadpos.contains(newstate.pos(headid)))
            vvvalidHeads.foreach(x => {
              vnPredictions.append(parser.decoding.PredictVN(newstate, "VVIRT", x))
            })
            val vnvalidHeads =
              if (nvirtheadpos == null)
                List()
              else
                possibleHeads.filter(headid => nvirtheadpos.contains(newstate.pos(headid)))
            vnvalidHeads.foreach(x => {
              vnPredictions.append(parser.decoding.PredictVN(newstate, "NVIRT", x))
            })
          }
        })
        attachList.appendAll(vnPredictions)
      }
      logger.debug("Created " + attachList.length + " new states")
      var assertsEnabled = false
      assert({assertsEnabled = true; true})
      //if (assertsEnabled) {
      //  attachList.foreach(x => assert(ShiftBasedTransitionParser.isTree(x),
      //    "not a tree: " + x +" with heads: " + x.heads.mkString(", ")))
      //}
      // attachList.foreach(state => state.sortVNs())
      attachList.toStream
    })

    //logger.info("All in all: " + result.length + "elements in stream")
    if (result.isEmpty) {
      // Something really went wrong keep going somehow and notify user.
      logger.warn(s"No legal transition found for new word ${newWord} with previous states: ${previousStates}!\n"+
      "attaching word everywhere...")
      val prevbest = previousStates.head
      return (0 to prevbest.lastWord).map(x => {
        parser.decoding.AttachNew(prevbest, newWord, newLemma, newPos, newCPos, x)
      }).toStream
    }
    result

  }

  def getEmptyState() = emptyState

  def doStep(states: collection.Seq[TAGLikeState], word: String, pos:String, lemma: String): Seq[TAGLikeState] = ???
}
