package parser.evaluation

import org.eclipse.collections.api.list.primitive.ImmutableIntList
import org.eclipse.collections.impl.factory.primitive.IntLists
import org.eclipse.collections.impl.list.primitive.IntInterval
import parser.DependencyInstance
import parser.AbstractTransitionParser.VN_UNMAPPED

import scala.collection.mutable.ArrayBuffer

object Matching {

  type MatchWithCount = (Array[Int], Int)

  /**
    *
    * @param goldHeads the gold standard heads
    * @param predHeads the predicted heads
    * @param matchingWithCount already performed matches; nodes without match have a value of -1
    *                 matching(0) == prediction node with index == lastWord+1
    * @param currElem prediction node we will try to match
    * @return a sequence of matchings with their corresponding added new correct attachments
    */
  def extendMatching(goldHeads: Seq[Int], predHeads: Seq[Int],
                     matchingWithCount: MatchWithCount, currElem: Int,
                     lastWord: Int): Seq[MatchWithCount] = {
    // which prediction node should be mapped?
    val matching = matchingWithCount._1
    val previousCorrect = matchingWithCount._2
    // go through each word outside the current prefix for a possible matching to currElem
    // and count the number of correct attachments created by this match
    (lastWord+1 until goldHeads.length)
      // only match to elements not previously matched to
      .withFilter(!matching.contains(_))
      // count number of new correct attachments
      .map{ (goldMatch) =>
      // correct if the head is in the prefix and it matches the gold standard
      val goldHead = goldHeads(goldMatch)
      var numNewCorrectAttachments = if (goldHead <= lastWord
        && goldHead == predHeads(currElem)) 1 else 0
      // if the head is outside the prefix and the head of the current element
      // is matched to head head in the gold standard.
      var predictedHead = predHeads(currElem)
      if (predictedHead > lastWord)
        predictedHead = matching(predictedHead - (lastWord+1))
      if (goldHead > lastWord && predictedHead == goldHead)
        numNewCorrectAttachments += 1
      // if a word in the prefix is attached to the matched word
      for (idx <- 1 to lastWord) {
        if (goldHeads(idx) == goldMatch && predHeads(idx) == currElem)
          numNewCorrectAttachments += 1
      }
      // if a matched prediction node is attached to this node
      var idx = 0
      while (idx < matching.length) {
        val alreadyMatched = matching(idx)
        // only look at matches already performed
        if (alreadyMatched > -1 &&
          // correct if the gold standard head is the matched word...
          goldHeads(alreadyMatched) == goldMatch &&
          // ... and the predicted head of that match is the current prediction node
          predHeads(lastWord + 1 + idx) == currElem) {
          numNewCorrectAttachments += 1
        }
        idx += 1
      }
      if (numNewCorrectAttachments == 0) {
        None // we just got a completely unhelpful attachment. No need to pursue further
        // we can still generate a matching with this element unmatched by starting from another
        // prediction node.  If all node matchings fail like this, we have no anchoring to the
        // existing prefix and don't want that matching anyhow.
      } else {
        val newMatching = matching.clone()
        newMatching(currElem-lastWord-1) = goldMatch
        Some(newMatching, numNewCorrectAttachments + previousCorrect)
      }
    }.collect({case Some(x) => x})
  }

  def bestMatchRec(goldHeads: Seq[Int], predHeads: Seq[Int], lastWord: Int, alreadyMatched: List[Int], matchings: Seq[MatchWithCount]): Seq[MatchWithCount] = {
    val numPredictionNodes = predHeads.length -1 - lastWord
    // try each permutation of attachment orderings and generate mappings
    // val bestMatchArr: Array[Int] =
    val unMatched = (lastWord+1 until predHeads.length).filterNot(alreadyMatched.contains(_))
    if (unMatched.isEmpty) {
      // nothing to match, return what we have
      return matchings
    }
    unMatched.flatMap(
      (elem) => {
        // try to match this element
        val newMatchings = matchings.
          flatMap(extendMatching(goldHeads, predHeads, _, elem, lastWord))
        if (newMatchings.isEmpty)
          // no new matches found, return what we have
          return matchings
        else
          // the new matchings are all better than the ones in matchings, therefore return the extensions of the new ones
          return bestMatchRec(goldHeads, predHeads, lastWord, elem :: alreadyMatched, newMatchings)
    })
  }

  def bestMatch(goldHeads: Seq[Int], predHeads: Seq[Int], lastWord: Int): Array[Int] = {
    val numPredictionNodes = predHeads.length -1 - lastWord
    val initialMatchings = List((Array.fill(numPredictionNodes)(VN_UNMAPPED), 0))
    (0 to lastWord).toArray ++ bestMatchRec(goldHeads, predHeads, lastWord, List(), initialMatchings).maxBy(_._2)._1
  }

  /*
    val bmOld = bestMatchOld(gold, predicted).toArray
    if (!bmOld.toArray.sameElements(bm)) {
      print(
        s"""diverging matches:
           |new match: ${bm.mkString(", ")}
           |old match: ${bmOld.mkString(", ")}
           |gold standard heads: ${gold.heads.mkString(", ")}
           |predicted heads: ${predicted.heads.mkString(", ")}
           |sentence: ${gold.forms.mkString(" ")}
        """.stripMargin)
    }
   */


  /**
    * Insert an element at the start of an immutable intlist
    */
  @inline
  def prepend(x: Int, l:ImmutableIntList) : ImmutableIntList = {
    val oldsize = l.size()
    val newArr = Array.ofDim[Int](oldsize+1)
    var i = 0
    while (i<oldsize) {
      newArr(i+1) = l.get(i)
      i += 1
    }
    newArr(0) = x
    IntLists.immutable.of(newArr:_*)
  }

  /**
    * Matches predicted to gold, result[predicted] == gold
    */
  def allMatches(gold: DependencyInstance, predicted: DependencyInstance, lastWord: Int, currentVN: Int) : ArrayBuffer[ImmutableIntList] = {
    if (currentVN >= predicted.heads.length) {
      val result: ArrayBuffer[ImmutableIntList] = new ArrayBuffer()
      result += IntLists.immutable.empty()
      return result
    }
    val result: ArrayBuffer[ImmutableIntList] = new ArrayBuffer()
    val partialMatches = allMatches(gold, predicted, lastWord, currentVN + 1)
    val currentType = predicted.postags(currentVN)
    partialMatches.foreach((x: ImmutableIntList) => {
      val allPossibleMatches = if (lastWord == gold.heads.length -1) IntLists.immutable.empty() else IntInterval.fromTo(lastWord+1, gold.heads.length-1)
      val nonTakenMatches = allPossibleMatches.newWithoutAll(x)
      // … which are of the same type as the VN we want to map (or is some untranslated PoS for which we don't know
      // whether it should be noun or verb
      // TODO maybe this is an ugly hack?
      val possibleMatches = nonTakenMatches .select((pmatch: Int) => gold.postags(pmatch).equals(currentType)
        || (gold.postags(pmatch) != "VVIRT"
        && gold.postags(pmatch) != "NVIRT")  )
      // can attach to VN_UNMAPPED (not mapped) and all not yet mapped gold nodes…
      prepend(VN_UNMAPPED, possibleMatches).each((elem: Int) => result.append(prepend(elem, x)))
    })
    return result
  }


  /**
    * Returns the number of incorrect attachments for a match. Returns
    * Double because we want to slightly prefer larger matches to
    * smaller ones.
    */
  def evalMatch(gold: DependencyInstance, predicted: DependencyInstance, pmatch: ImmutableIntList, lastWord: Int): Double = {
    var result = 0.0
    // it is good if a normal word has the correct head
    assert(predicted.heads.length > lastWord, "pred-gold mismatch: "+gold.forms.mkString(" ")
      + gold.heads.mkString("/") + predicted.forms.mkString(" ") + predicted.heads.mkString("/"))
    var i = 1; while (i <= lastWord) {
      if (predicted.heads(i) > lastWord
        // pmatch is 0-based VNs only, heads are one-based all words
        && gold.heads(i) == pmatch.get(predicted.heads(i))) {
        result += 1
      }
      i += 1
    }
    i = lastWord + 1; while (i < pmatch.size()) {
      if (pmatch.get(i) > 0 && pmatch.get(predicted.heads(i)) == gold.heads(pmatch.get(i)))
        result += 1
      if (pmatch.get(i) == VN_UNMAPPED) //nothing matched
        result += 0.01 // TODO make sure no match is better than completely wrong match?
      i += 1
    }
    result
  }

  /**
    * produces a match from the predicted words and VNs of prediced to gold
    */
  def bestMatchOld(gold: DependencyInstance, predicted: DependencyInstance): ImmutableIntList = {
    val lastWord = predicted.lastNormalWord
    val matches = allMatches(gold, predicted, lastWord, lastWord+1)
    if (matches.isEmpty) {
      return IntInterval.zeroTo(gold.heads.length - 1)
    }
    // evaluate all matches. a match only maps predicted nodes but
    // evalMatch takes a complete list -> pad with parallel mapping
    // for the existing words
    val bestmatch = matches.maxBy((m) => {evalMatch(gold, predicted, IntInterval.zeroTo(lastWord).newWithAll(m), lastWord)})
    IntInterval.zeroTo(lastWord).newWithAll(bestmatch)
  }
}
