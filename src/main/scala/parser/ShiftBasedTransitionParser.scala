/* package parser

import java.util
import java.util.concurrent._
import java.util.concurrent.atomic.AtomicInteger

import com.google.common.collect.MinMaxPriorityQueue
import com.google.common.util.concurrent.AtomicDouble
import com.typesafe.scalalogging.Logger
import gnu.trove.list.array.TIntArrayList
import gnu.trove.map.hash.THashMap
import gnu.trove.set.hash.THashSet
import org.slf4j.LoggerFactory
import parser.decoding.BaseState

import scala.collection.mutable

// import parser.InstanceData

import scala.collection.JavaConversions._

/**
 * Created by koehn on 6/17/15.
 */

object ShiftBasedTransitionParser {

  val logger = Logger(LoggerFactory.getLogger("ShiftBasedTransitionParser"))
  /**
   * Predicate to check Tree-ness of state
   */
  def  isTree[T<:BaseState[T]](state: BaseState[T]) : Boolean = {
    logger.debug("isTree " + state)
    val heads = state.toInstanceUninstantiated.heads
    // luckily, heads[0] == 0 (that value is only for padding)

    if (! heads.forall(x => x > -1))
      return false

    val root = heads.tail.indexOf(0) + 1
    if (root < 1 || heads.lastIndexOf(0) > root) {
      // either none or too many roots
      return false
    }
    // recurse over the tree top-down
    def is_real_tree(node : Int, visited: mutable.Set[Int]): Boolean = {
      logger.debug("visited: " + visited)
      // get children of node
      val newNodes = heads.zipWithIndex.filter(_._1 == node).map(_._2).toSet
      if ((visited & newNodes).nonEmpty) {
        return false
      }
      visited ++= newNodes
      newNodes.foreach( (x) => {
        if (!is_real_tree(x, visited))
          return false
      }
      )
      return true
    }
    logger.debug("simple checking done")
    val visited = mutable.Set[Int](root)
    return is_real_tree(root, visited) && visited.size == heads.length - 1
  }

  /**
   * Filter States that have already been visited.
   *  Extra function for debugging purposes
   * TODO reintegrate
   */
  @inline
  def myfilter(x: TransitionState, alreadyProduced: util.HashSet[TIntArrayList]): Boolean = {
    val uIntList = x.uniqueIntList()
    val result = uIntList == null || alreadyProduced.add(x.uniqueIntList())
    result
  }

  /**
   * Returns a Stream of states that represent complete trees and can
   * be reached from states.  alreadyProduced takes track of states
   * already in the stream to prevent duplicates
   */
  def produce_final_states(states: Stream[TransitionState],
                           alreadyProduced: util.HashSet[TIntArrayList],
                           possibleEdges: THashMap[String, THashSet[String]] = null): Stream[TransitionState] = {
    // only iterate over unseen states
    states.filter(myfilter(_, alreadyProduced)) flatMap ((state: TransitionState) => {
      // add every state that is a complete tree
      val newEle = if (! state.heads.contains(-1)) state else null

      //alreadyProduced.add(state.uniqueIntList())
      val successorStates = state.buildNewStates(possibleEdges)
      if (successorStates.size() != 0) {
        if (newEle == null) {
          produce_final_states(successorStates.toStream, alreadyProduced, possibleEdges)
        } else {
          newEle #:: produce_final_states(successorStates.toStream, alreadyProduced, possibleEdges)
        }
      } else { // successorStates.size == 0
        if (newEle == null) {
          Stream.empty
        } else {
          Stream(newEle)
        }
      }
    }) // end flatMap
  }

}

class ShiftBasedTransitionParser(beamSize: Int)
      extends AbstractTransitionParser[TransitionState](beamSize, TransitionState.getEmptyState) {

  var bestStatePerRollback: util.HashMap[TIntArrayList, Double] = null;

  override def generateNewStateStream(previousStates: Iterable[TransitionState],
                                      newWord: String,
                                      newPos: String,
                                      newLemma: String): Stream[TransitionState] = {
   // backtrackExtendFinalize(states: Stream[TransitionState], word:String, pos: String, lemma: String,
   //   possibleEdges: THashMap[String, THashSet[String]] = null)

    backtrackExtendFinalize(previousStates.toStream, newWord, newPos, newLemma, possibleEdgesBetweenPOS)
  }

  override def setupBeforeTrainRunners() = {
    bestStatePerRollback = new util.HashMap()
  }

  override def buildTrainRunner(
    goldInstance: DependencyInstance,
    trainStructs: MinMaxPriorityQueue[TrainStruct[TransitionState]],
    worstScore: AtomicDouble,
    newStatesQueue: ArrayBlockingQueue[Option[TransitionState]],
    tp: AbstractTransitionParser[TransitionState],
    beamSize: Int,
    bestStruct: TrainStructRef[TransitionState],
    bestStructLock: String,
    bestScore: AtomicInteger
  ): TrainRunner[TransitionState] = new TrainRunnerShift (
    goldInstance,
    trainStructs,
    worstScore,
    newStatesQueue,
    tp,
    beamSize,
    bestStruct,
    bestStructLock,
    bestScore,
    bestStatePerRollback)
    //override def generateNewStateStream[TransitionState](previousStates: Iterable[TransitionState], newWord: String,
    //newPos: String, newLemma: String): Stream[TransitionState] = {
    //        backtrackExtendFinalize(previousStates.toStream, newWord, newPos, newLemma)



  def doStep(states: collection.Seq[TransitionState], word: String, pos:String, lemma: String) =
    clipBeam(backtrackExtendFinalize(states.toStream, word, pos, lemma).toSeq)


  /**
    * Rolls back all states to last Rollback point, extends with word
    * and then generates all final states
    * @return a stream of new states
    */
  def backtrackExtendFinalize(states: Stream[TransitionState], word:String, pos: String, lemma: String,
                               possibleEdges: THashMap[String, THashSet[String]] = null)
      : Stream[TransitionState] = {
    ShiftBasedTransitionParser.produce_final_states(states.map((x) => x.rollBackTo).map(
      (s: TransitionState) => {s.extendWords(List(word),List(pos), List(lemma))}
    ),
      new util.HashSet[TIntArrayList](),
      possibleEdgesBetweenPOS
    )
  }

  def getEmptyState() = TransitionState.getEmptyState()

}

*/