package parser.logging
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import parser.decoding.BaseState

import scala.collection.mutable.ArrayBuffer
import scalatags.Text.all.Frag


class HTMLTracer[T<:BaseState[T]] extends HTMLLogger[T] {
  // override val logger = Logger(LoggerFactory.getLogger("HTMLTracer"))

  // always write output on sentence change
  override def shouldWriteOutput: Boolean = true

  override protected def sentenceOutput(): ArrayBuffer[Frag] = {
    val output = new ArrayBuffer[Frag]()
    for (increment <- increments) {
      output.append(html_for_increment(increment))
    }
    output
  }

}