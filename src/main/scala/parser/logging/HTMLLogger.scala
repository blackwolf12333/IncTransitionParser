package parser.logging

import java.io._
import java.util

import com.typesafe.scalalogging.Logger
import io.gitlab.nats.deptreeviz
import io.gitlab.nats.deptreeviz.{DepTree, SimpleParse, SimpleWord}
import org.eclipse.collections.api.list.primitive.ImmutableIntList
import org.slf4j.LoggerFactory
import parser.decoding.{BaseState, TAGLikeState}
import parser.{AttachmentErrors, DependencyInstance, TrainStruct}

import scala.collection.mutable.ArrayBuffer
import scalatags.Text.TypedTag
import scalatags.Text.all._
import collection.JavaConverters._

/**
  * A logger to HTML files, providing two modes: error logging and tracing.
  * By default, only increments where the amount of errors rises are logged.
  * tracing logs all transitions.
  * Created by arne on 23.03.17.
  */
class HTMLLogger[T<:BaseState[T]] {
  val logger = Logger(LoggerFactory.getLogger("HTMLLogger"))
  val dt = new DepTree[SimpleParse, SimpleWord]()
  private var curOutIndex = 0
  private val outDir = "/tmp/transitionlogs/"

  sealed case class IncrementInformation(bestScoring: TrainStruct[T], leastErrors: TrainStruct[T],
                                         gold: DependencyInstance,
                                         bsmapping: Array[Int], lemapping: Array[Int])

  protected val increments: ArrayBuffer[IncrementInformation] = new ArrayBuffer[IncrementInformation]()
  new File(outDir).mkdirs()
  private var currOutput: BufferedWriter = null
  switchFile()
  private val currHtml = new ArrayBuffer[Frag]()

  private def switchFile() = {
    logger.warn("switching file")
    curOutIndex += 1
    currOutput = new BufferedWriter(new FileWriter(outDir + s"sentence_${curOutIndex}.html"))
  }

  def logIncrement(bestScoring: TrainStruct[T], leastErrors: TrainStruct[T], gold: DependencyInstance, bsmapping: Array[Int], lemapping: Array[Int]) = {
    increments.append(IncrementInformation(bestScoring, leastErrors, gold, bsmapping, lemapping))
  }

  /**
    * whether to write output (called when switching to next sentence)
    */
  protected def shouldWriteOutput: Boolean = currHtml.size > 10

  /**
    * Generates HTML for the sentence increments, clears buffers and rotates the log if needed
    */
  def newSentence() = {
    logger.trace("new sentence")
    currHtml.appendAll(sentenceOutput())
    increments.clear()
    if (shouldWriteOutput) {
      write_html()
    }
  }

  private def write_html() = {
    val all_html = html(
      head(meta(charset:="utf-8")),
      body(currHtml))
    currOutput.write(all_html.toString())
    currHtml.clear()
    switchFile()
  }

  protected def sentenceOutput(): ArrayBuffer[Frag] = {
    var prefLeastErrors = 0
    var i = 0
    val output = new ArrayBuffer[Frag]()
    var prev_was_printed = true
    while (i < increments.size) {
      if (increments(i).leastErrors.numErrors.unlabeled > prefLeastErrors + 1) {
        prefLeastErrors = increments(i).leastErrors.numErrors.unlabeled
        if (!prev_was_printed) {
          output.append(html_for_increment(increments(i-1)))
        }
        output.append(html_for_increment(increments(i)))
        prev_was_printed = true
      } else {
        prev_was_printed = false
      }
      i += 1
    }

    output
  }

  var svgcounter = 0
  protected def html_for_increment(inc: IncrementInformation): TypedTag[String] = {
    svgcounter += 1
    val goldsvgpath = outDir+s"gold_${svgcounter}.svg"
    val leasterrsvgpath = outDir+s"leasterr_${svgcounter}.svg"
    val bestscoresvgpath = outDir+s"bestscore_${svgcounter}.svg"
    svg_for_conll(inc.gold.toString, new FileWriter(goldsvgpath))
    svg_for_conll(inc.leastErrors.state.getInstance.toString, new FileWriter(leasterrsvgpath))
    svg_for_conll(inc.bestScoring.state.getInstance.toString, new FileWriter(bestscoresvgpath))
    div(p(inc.gold.forms.mkString(" ")),
      table(
        tr(
          td(pre(inc.gold.toString())),
          td(pre(inc.leastErrors.state.getInstance.toString())),
          td(pre(inc.bestScoring.state.getInstance.toString()))
        ),
        tr(
          td(),
          td(inc.lemapping.toString),
          td(inc.bsmapping.toString)
        ),
        tr(
          td(),
          td(inc.leastErrors.numErrors.toString()),
          td(inc.bestScoring.numErrors.toString())
        ),
        tr(
          td(img(src:=goldsvgpath)),
          td(img(src:=leasterrsvgpath)),
          td(img(src:=bestscoresvgpath))
        ),
        tr(
          td(),
          td(inc.leastErrors.state.toString()),
          td(inc.bestScoring.state.toString())
        )
      )
    )
  }

  private def svg_for_conll(conll: String, w: Writer) = {
    val p = SimpleParse.fromConll(conll.split("\n").toBuffer.asJava)
    dt.setDecParse(p)
    dt.draw(p)
    dt.writeTree(w)
  }
}
