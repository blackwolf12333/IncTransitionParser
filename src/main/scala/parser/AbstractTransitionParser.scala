package parser

import java.io._
import java.util.Comparator
import java.util.concurrent._
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

import com.github.benmanes.caffeine.cache.{Caffeine, LoadingCache}
import com.google.common.base.{Stopwatch, Throwables}
import com.google.common.collect.MinMaxPriorityQueue
import com.google.common.util.concurrent.{AtomicDouble, MoreExecutors, ThreadFactoryBuilder}
import com.typesafe.scalalogging.Logger
import gnu.trove.map.hash.THashMap
import gnu.trove.set.hash.THashSet
import org.slf4j.LoggerFactory
import parser.Options.LearningMode
import parser.decoding.{BaseState, IncHillClimbingDecoder}
import parser.evaluation.Matching.bestMatch
import parser.io._
import parser.logging.{HTMLLogger, HTMLTracer}
import parser.scoring.{ErrorComputer, LstmScorer, LstmScoringRunner, RBGScorer}
import utils.FeatureVector

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.compat.java8.FunctionConverters._
import scala.concurrent._

/*

The AbstractTransitionParser provides the following structure:
 - holds a set of current states
 - push new word into parser
 - call abstract generateNewStateStream for a stream of the new states
 - computes the score for each new state
 - forms a beam out of the best-scoring states
 - repeat

Implemented by subclasses:
 - which states to actually use
 - generateNewStateStream

 */


object AbstractTransitionParser {
  val VN_UNMAPPED = -2;
  val VN_NAME = "[virtual]"
  val logger = Logger(LoggerFactory.getLogger("TransitionParser"))
  val transitionErrorLogger = Logger(LoggerFactory.getLogger("TransitionErrors"))

  def lastNormalWord(forms: Seq[String]): Int = {
    val firstVN = forms.indexWhere(_.contains(VN_NAME))
    // val firstVN = forms.indexOf(TransitionState.VN_NAME)
    if (firstVN > -1)
      firstVN - 1
    else
      forms.length - 1 // no VN found, take last index
  }

  /*
   *  switch positions of gold instance VNs so they match the predicted order (since we don't
   *  enforce ordering of VNs, wrong ordering shouldn't be an error and for the training both
   *  should be aligned)
   *  ??? I don't remember what this comment was for.
   */




  /*
  def bestMatchIntelligent(gold: DependencyInstance, predicted: DependencyInstance): ImmutableIntList = {
    val lastWord = predicted.lastNormalWord
    // we iterate through the prediction nodes.  If a prediction node can somehow be correctly anchored in the prefix (or the root),
    // count the resulting number of correct attachments and keep it as a possibility.
    // iterate through each permutation of prediction nodes as we don't know in which ordering they are anchored.
    (lastWord+1 until predicted.length).permutations.flatMap((ordering) => {
      // matchings for the current
      val matchingsSoFar: ArrayBuffer[ArrayBuffer[Int]] = new ArrayBuffer[ArrayBuffer[Int]]()
      // get all possible mappings for #correct mappings for each prediction node
      for (prediction <- ordering) {
        for (goldMatch <- lastWord until gold.length) {
          // correct if the head is in the prefix and it matches the gold standard
          var numNewCorrectAttachments = if (gold.heads(goldMatch) <= lastWord && gold.heads(goldMatch) == predicted.heads(prediction)) 1 else 0
          // if a word in the prefix is attached to the matched word
          for (idx <- 1 to lastWord) {
            if (gold.heads(idx) == goldMatch && predicted.heads == prediction)
              numNewCorrectAttachments += 1
          }
          // if a matched prediction node is attached to this node
          // TODO
          if (numNewCorrectAttachments > 0)
            // some contribution -> save for later
        }
        val validMatches = (lastWord until gold.length).filter((idx) => {

          gold.heads(idx) == prediction
        })
      }
      List()
    } )
    return null
  }
*/

  /**
    * Computes Instances suitable for training by aligning words,
    * inserting dummy words for padding etc.
    *
    * @returns adjusted instances
    */
  def createTrainingInstances(fear: DependencyInstance, gold: DependencyInstance, matching: Array[Int]) :
      (DependencyInstance, DependencyInstance) = {
    val lastWord = lastNormalWord(fear.forms)

    val fearData = new InstanceData(lastWord)
    val goldData = new InstanceData(lastWord)

    // Number of unmapped virtual nodes up to a specific point
    // val num_unmapped_gold: ArrayBuffer[Int] = new ArrayBuffer()
    val numgold = gold.heads.length - 1
    val numfear = fear.heads.length - 1
    val maxIndex = math.max(numgold, numfear)
    val num_unmapped_gold = mutable.ArrayBuffer.fill(maxIndex + 1)(0)

    val unmapped_gold = (1 to numgold).filter(matching.indexOf(_) == -1)

    unmapped_gold.foreach((unm) =>
      (unm to maxIndex).foreach(num_unmapped_gold(_) += 1))

    // unmapped gold will be placed infront of each fear with same index. therefore,
    // fear heads are moved back exactly the number of unmapped gold until fearid.
    val newFearHeads = fear.heads.map(head =>
      if (head < 0)
        head
      else
        head + num_unmapped_gold(head)
    )

    val newGoldHeads = gold.heads.map(head => {
      val mhead = matching.indexOf(head)
      if (mhead == -1) {
        // unmapped heads are moved (see above), but only as much as there are fear heads.
        // example: fear = a gold = x,y,z, mapping empty:
        // new serialization: x a y z
        // notice that z is only moved one but it has two unmapped nodes in front
        val adjustment = math.max(0, head - numfear - 1)
        // head <= 0: head ==-1 (no head) is not in the matching but should obyiously stay as no match.
        if (head <= 0) head else head + num_unmapped_gold(head - 1) - adjustment
      } else {
        // the head is computed just as fear (because it is mapped to fear)
        mhead + num_unmapped_gold(mhead)
      }
    })
/*    System.err.println("fear.heads " + fear.heads.mkString("/"))
    System.err.println("newFearHeads " + newFearHeads.mkString("/"))
    System.err.println("newGoldHeads " + newGoldHeads.mkString("/"))
    System.err.println("num_unmapped_gold " + num_unmapped_gold.mkString("/"))
*/

    // fill normal words
    (0 to lastWord).foreach ((i) => {
      fearData.doAppend(fear, i, newFearHeads(i))
      goldData.doAppend(gold, i, newGoldHeads(i))
    })

    // fill VNs
    (lastWord + 1 to maxIndex) foreach ((i) => {
      if ( i <= numgold) {
        if (matching.indexOf(i) == -1) {
          fearData.doAppend(gold, i, VN_UNMAPPED)
          goldData.doAppend(gold, i, newGoldHeads(i))
        } // else nothing needs to be done
      }
      if (i <= numfear) {
        fearData.doAppend(fear, i, newFearHeads(i))
        if (matching(i) == VN_UNMAPPED) {
          goldData.doAppend(fear, i, VN_UNMAPPED)
        } else {
          val corrGold = matching(i)
          goldData.doAppend(gold, corrGold, newGoldHeads(corrGold))
        }
      }
    })
    return (fearData.toInstance, goldData.toInstance)
  }

  /**
   * Computes the set of all possible head POS for given dependent POS
    *
    * @param file
   * @param options in fact not needed, but something downstream wants it anyway
   * @return
   */
  def createEdgeFilterSet(file: String, options: Options, parserConf: ParserConf): THashMap[String, THashSet[String]] = {
    // first key == dep, set elements == possible heads
    val result = new THashMap[String, THashSet[String]]()
    val nonIncInput = !parserConf.inputIsIncrements()
    val vvirtSet = new THashSet[String]()
    if (nonIncInput) {
      result.put("VVIRT", vvirtSet)
      vvirtSet.add("VVIRT")
    }
    val reader: DependencyReader = DependencyReader.createDependencyReader(options)
    reader.startReading(file)
    var inst: DependencyInstance = reader.nextInstance
    var num = 0
    while (inst != null) {
      {
        var i: Int = 1
        while (i < inst.length) {
          val headSet: THashSet[String] = result.get(inst.postags(i)) match {
            case null => {val emptySet = new THashSet[String]; result.put(inst.postags(i), emptySet); emptySet}
            case x => x
          }
          headSet.add(inst.postags(inst.heads(i)))
          if (nonIncInput) {
            // we have to manually infer what can be attached to prediction nodes
            if(inst.heads(i)>i) // head might be in the future: add VVIRT as possible head
              headSet.add("VVIRT")
            // everything that has dependents should also be able to have prediction nodes as dependents.
            vvirtSet.add(inst.postags(inst.heads(i)))
          }
          i += 1
        }
      }
      inst = reader.nextInstance
      num += 1
      if (num % 10000 == 0)
        logger.trace(s"edgefilter at ${num}")
    }
    reader.close
    logger.info("Edgefilter created")
    // result.keySet().forEach((x)=> System.out.println(s"$x : ${result.get(x).toString}"))
    result
  }
}

abstract class AbstractTransitionParser[T<:BaseState[T]](val conf: ParserConf, emptyState: T) {
  import AbstractTransitionParser._

  // first key == dep, set elements == possible heads
  var possibleEdgesBetweenPOS: THashMap[String, THashSet[String]] = null

  val depParser = new DependencyParser()
  var lstmScorer: LstmScorer = null
  var options: Options = new Options()
  if (!conf.modelFile.isSupplied) {
    options.processArguments(Array("test-file:gibberish"))
    options.processArguments(conf.rbgOptions().toArray)
    setUpDepParser(conf.trainFile())
  } else {
    loadModel()
    options = depParser.pipe.options
    options.modelFile = conf.modelFile()
  }
  depParser.options = options

  val beamSize = conf.beamSize()

  var errorStats = new ErrorStatsCollector[T](conf.evalFrontierSize())
  var bestErrorStats = new ErrorStatsCollector[T](conf.evalFrontierSize())

  // instance and pipe are not existent at this point
  val _fvCacheLoader = new IncLocalFeatureCacheLoader(null, null)
  val _scoreCacheLoader = new IncScoreCacheLoader(null)

  //**********************************************************************
  // BEGIN Caches
  //
  // the featureCache holds pairs of Inc{local,global}featuredata because different
  // trees share the same data if they have the same nodes
  val _featureCache = Caffeine.newBuilder().
    maximumSize(30).
//    recordStats().
    build[String, (IncLocalFeatureData, IncGlobalFeatureData)]// new mutable.HashMap[String, Future[(IncLocalFeatureData, GlobalFeatureData)]]
  // stores feature vectors for features such as grandparent etc. May be reused by later increments
  // only stores in-prefix features
  val _fvCache : LoadingCache[LocalFeatureCacheKey, FeatureVector] = Caffeine.newBuilder().
    maximumSize(5000).
//    recordStats().
    build[LocalFeatureCacheKey, FeatureVector](_fvCacheLoader)
  // stores the scores corresponding to the features.  Invalidated during training between increments as
  // training updates change the scores
  val _scoreCache: LoadingCache[LocalFeatureCacheKey, java.lang.Double] = Caffeine.newBuilder().
    maximumSize(5000).
 //   recordStats().
    build[LocalFeatureCacheKey, java.lang.Double](_scoreCacheLoader)

  // END Caches
  //**********************************************************************

  var _trainIter = 0

  val errorLogger = if (conf.htmlTrace()) new HTMLTracer[T] else new HTMLLogger[T]()

  val _numThreads = conf.numThreads()

  //**********************************************************************
  // BEGIN Thread pools
  val rbgScoringThreadPool: ExecutorService = MoreExecutors.getExitingExecutorService(
    Executors.newFixedThreadPool(_numThreads,
                                 new ThreadFactoryBuilder().setNameFormat("rbgscorer-%d").build()
                                ).asInstanceOf[ThreadPoolExecutor],
    0,
    TimeUnit.SECONDS
  )
  val lstmScoringThreadPool: ExecutorService = MoreExecutors.getExitingExecutorService(
    Executors.newFixedThreadPool(1,
      new ThreadFactoryBuilder().setNameFormat("lstmscorer-%d").build()
    ).asInstanceOf[ThreadPoolExecutor],
    0,
    TimeUnit.SECONDS
  )
  val errorComputerThreadPool: ExecutorService = MoreExecutors.getExitingExecutorService(
    Executors.newFixedThreadPool(_numThreads,
      new ThreadFactoryBuilder().setNameFormat("errorcomputer-%d").build()
    ).asInstanceOf[ThreadPoolExecutor],
    0,
    TimeUnit.SECONDS
  )
  val newStatesPool: ExecutorService = MoreExecutors.getExitingExecutorService(
    Executors.newFixedThreadPool(1,
      new ThreadFactoryBuilder().setNameFormat("newstatesrunner-%d").build()
    ).asInstanceOf[ThreadPoolExecutor],
    0,
    TimeUnit.SECONDS
  )
  // BEGIN Thread pools
  //**********************************************************************



  // Stuff for analyzing behaviour
  val singleThreadSW =  Stopwatch.createStarted()
  val multiThreadSW =  Stopwatch.createUnstarted()

  var lastLeastErrorsWeighted = 0.0

  /**
    * sets all global caches into a working state using the given instance
    * Does not invalidate the caches.
    * @param instance
    */
  def prepare_caches(instance: DependencyInstance) = {
    _fvCacheLoader.inst = instance
    _scoreCacheLoader.lfd = new IncLocalFeatureData(instance, depParser, false, _fvCache, _scoreCache, instance.lastNormalWord)
    logger.trace(s"score cache hit rate: ${_scoreCache.stats().hitRate().toString}")
    logger.trace(s"Fv cache hit rate: ${_fvCache.stats().hitRate().toString}")
    // logger.trace(_scoreCache.stats().toString)
  }

  /**
    * Main train function. Trains on trainfile
    */
  def train(trainFile : String): Unit = {
    _trainIter = 0
    lstmScorer.isTraining = true
    _fvCacheLoader.synFactory = depParser.pipe.synFactory
    possibleEdgesBetweenPOS = createEdgeFilterSet(trainFile, options, conf)
    val overallSW = Stopwatch.createStarted()
    //val tensor: LowRankParam = new LowRankParam(depParser.parameters)
    //depParser.pipe.fillParameters(tensor, depParser.parameters)
    // tensor.decompose(1, depParser.parameters)
    // TODO Wann decompose?

    var lastAccuracy = 0.0f
    var currIteration = 1
    var improvedLastIteration = true
    while (currIteration <= conf.numIterations() && improvedLastIteration) {
      val incrementSupplier = IncrementSupplier.getSupplier(options, conf, conf.trainFile())
      processData(incrementSupplier, train = true)
      logger.info(s"run $currIteration of ${conf.numIterations()} finished")
      val newAccuracy = if (conf.validationFile.supplied) validate() else lastAccuracy + 0.1f
      logger.info("theta: " + lstmScorer.theta.values().toFloat().toString)
      if (lastAccuracy > newAccuracy)
        improvedLastIteration = false
      else
        saveModel()
      currIteration += 1
    }
    //Console.err.println("Cache hit rate: " +  _featureCache.stats().hitRate())
  }

  /**
    * initializes the DependencyParser for training.
    * @param trainFile
    */
  def setUpDepParser(trainFile: String): Unit = {
    options.R = 5
    options.gamma = 1.0
    options.gammaLabel = 1.0
    options.maxNumIters = options.numPretrainIters
    options.learnLabel = true
    depParser.pipe = new DependencyPipe(options)
    depParser.pipe.createAlphabets(trainFile)

    depParser.parameters = new Parameters(depParser.pipe, options)
    depParser.parameters.gamma = 1.0
    depParser.parameters.gammaLabel = 1.0
    lstmScorer = new LstmScorer(depParser.pipe, conf)
  }

  def test() = {
    _trainIter = 0
    lstmScorer.isTraining = false
    val incrementSupplier = IncrementSupplier.getSupplier(options, conf, conf.testFile())
    errorStats = new ErrorStatsCollector[T](errorStats.frontierSize)
    bestErrorStats = new ErrorStatsCollector[T](errorStats.frontierSize)
    _fvCacheLoader.synFactory = depParser.pipe.synFactory
    val overallSW = Stopwatch.createStarted()
    processData(incrementSupplier, train = false)
    println("=== Test results ===")
    println("Predicted accuracy:")
    errorStats.printStats()
    println("Best in beam accuracy:")
    bestErrorStats.printStats()
    logger.info(s"Time in singlethread: ${singleThreadSW} in multithread: ${multiThreadSW}" +
                s", per increment: ${overallSW.elapsed(TimeUnit.MILLISECONDS)/_trainIter}")
  }

  /**
    * validates the current model against validation data, returns a score.
    * Assumes that there is a current model already loaded (because you are currently training one)
    */
  def validate(): Float = {
    lstmScorer.isTraining = false
    _trainIter = 0
    val incrementSupplier = IncrementSupplier.getSupplier(options, conf, conf.validationFile())

    _fvCacheLoader.synFactory = depParser.pipe.synFactory
    val overallSW = Stopwatch.createStarted()
    errorStats = new ErrorStatsCollector[T](errorStats.frontierSize)
    bestErrorStats = new ErrorStatsCollector[T](errorStats.frontierSize)
    processData(incrementSupplier, train = false)
    println("=== validation results ===")
    println("Predicted accuracy:")
    errorStats.printStats()
    println("Best in beam accuracy:")
    bestErrorStats.printStats()
    logger.info(s"Time in singlethread: ${singleThreadSW} in multithread: ${multiThreadSW}" +
      s", per increment: ${overallSW.elapsed(TimeUnit.MILLISECONDS)/_trainIter}")
    overallSW.reset()
    errorStats.numErrors.unlabeledWeighted.toFloat
  }

  def testWithIncHillClimbingDecoder() = {
    val incrementSupplier = IncrementSupplier.getSupplier(options, conf, conf.testFile())
    _fvCacheLoader.synFactory = depParser.pipe.synFactory
    val accCounter = new ErrorStatsCollector(conf.evalFrontierSize())
    val hcdec = new IncHillClimbingDecoder(conf, options, lstmScorer, _fvCache, _scoreCache)
    hcdec.setPossibleEdgesBetweenPOS(possibleEdgesBetweenPOS)
    val outputWriter = new Conll06Writer(options, depParser.pipe)
    if (conf.outFile.isSupplied)
      outputWriter.startWriting(conf.outFile())
    var i = 0
    for ((currPrefix: DependencyInstance, newLength: Int, newSentence: Boolean) <- incrementSupplier) {
      // newSentence is only true if there was another sentence before,
      // also catch the first sentence
      if (newSentence || i == 0) {
        currPrefix.setInstIds(depParser)
        _fvCache.invalidateAll()
        _scoreCache.invalidateAll()
      }
      val inst = currPrefix.shorten(newLength)
      prepare_caches(inst)
      val parse = hcdec.decode(inst, depParser)
      if (conf.outFile.isSupplied)
        outputWriter.writeInstance(parse)
      // evaluate
      val bm = bestMatch(currPrefix.heads, parse.heads, newLength)
      accCounter.addToStats(parse, currPrefix, newLength, bm)
      i += 1
      if (i % 100 == 0) {
        println(s"after ${i} increments:")
        accCounter.printStats()
      }
    }
    println("complete results:")
    accCounter.printStats()
  }


  def processData(incrementSupplier: IncrementSupplier, train: Boolean) = {
    var hcdec: IncHillClimbingDecoder = null
    if (train && conf.trainHillClimbing()) {
      hcdec = new IncHillClimbingDecoder(conf, options, lstmScorer, _fvCache, _scoreCache)
      hcdec.setPossibleEdgesBetweenPOS(possibleEdgesBetweenPOS)
    }

    val outputWriter = new Conll06Writer(options, depParser.pipe)
    if (conf.outFile.isSupplied)
      outputWriter.startWriting(conf.outFile())
    var previousStates = List(emptyState)
    var modelUpdatedThisSentence = false
    var updateLengths = new mutable.ArrayBuffer[Int]()
    for ((currPrefix: DependencyInstance, newLength: Int, newSentence: Boolean) <- incrementSupplier) {
      currPrefix.setInstIds(depParser)
      logger.trace(s" Num Elements in feature cache: ${_featureCache.estimatedSize()}")
      _featureCache.invalidateAll()
      lstmScorer.newPrefix(currPrefix, newLength)
      if (train) _scoreCache.invalidateAll()
      prepare_caches(currPrefix)
      if (newSentence) {
        if (conf.htmlErrorLog())
          errorLogger.newSentence()
        logger.trace("processed " + previousStates(0).fdInformation)
        logger.trace(_fvCache.stats().hitRate().toString + _fvCache.stats().toString)
        previousStates = List(emptyState)
        _fvCache.invalidateAll()
        modelUpdatedThisSentence = false
      }
      // if we already updated the model in this sentence and
      // updateOncePerSentence is set, we switch to evaluation mode for the rest of the sentence.
      val (modelUpdated, newStates) = processInstance(previousStates, currPrefix, train && ! modelUpdatedThisSentence, newLength)
      if (modelUpdated) {
        updateLengths += newLength
        if (updateLengths.size == 100) {
          logger.trace(s"Average update prefix length: ${updateLengths.sum / 100.0}")
          updateLengths.clear()
        }
      }
      modelUpdatedThisSentence = (modelUpdated || modelUpdatedThisSentence) && conf.updateOncePerSentences() && newLength > 2
      // select only the states, rewind them if unstable predictions activated and pick top afterwards.
      // TODO this can lead to duplicate elements in the beam
      def getprevmaybe(x: T): T =
      { if (! conf.unstablePredictions()) {return x}
        var res = x
        while (x.lastWord > 0 && res.getPreviousState.lastWord == x.lastWord) res = res.getPreviousState
      res}
      previousStates = newStates.toStream.map(_.state).map(getprevmaybe).slice(0, beamSize).toList
      if (conf.outFile.isSupplied)
        outputWriter.writeInstance(previousStates.head.getInstance)

      // perform an update w/hill climber if sentence is complete & training activated
      if (train && conf.trainHillClimbing() && newLength == currPrefix.length-1) {
        val predInst = hcdec.decode(currPrefix, depParser, 0)
        val ua = DependencyParser.evaluateUnlabelCorrect(predInst, currPrefix, true)
        if (ua < predInst.length - 1) {
          val lfd = new LocalFeatureData(currPrefix, depParser, false)
          val fvUpdate = lfd.getFeatureDifference(currPrefix, predInst)
          val gfd = new GlobalFeatureData(lfd)
          fvUpdate.addEntries(gfd.getFeatureDifference(currPrefix, predInst))
          val loss = depParser.parameters.updateWithFv(_trainIter, predInst.lastNormalWord - ua, fvUpdate)
        }

      }
    }
  }

  @throws[IOException]
  def saveModel() = {
    // Don't save the RBG model if we only want to posttrain the lstm model
    if (!conf.trainLstmOnly()) {
      val out: ObjectOutputStream =
        new ObjectOutputStream(new GZIPOutputStream(
          new FileOutputStream(conf.outModelFile())))
      out.writeObject(depParser.pipe)
      out.writeObject(depParser.parameters)
      out.writeObject(depParser.pipe.options)
      out.writeObject(possibleEdgesBetweenPOS)
      out.close()
    }
    lstmScorer.saveModel(conf.outModelFile())
  }

  @throws[IOException]
  @throws[ClassNotFoundException]
  def loadModel() = {
    val in: ObjectInputStream =
      new ObjectInputStream(new GZIPInputStream(
        new FileInputStream(conf.modelFile())))
    depParser.pipe = in.readObject.asInstanceOf[DependencyPipe]
    depParser.parameters = in.readObject.asInstanceOf[Parameters]
    depParser.pipe.options = in.readObject.asInstanceOf[Options]
    depParser.pipe.options.printOptions()
    depParser.parameters.options = depParser.pipe.options
    possibleEdgesBetweenPOS = in.readObject.asInstanceOf[THashMap[String, THashSet[String]]]
    if (conf.trainLstmOnly()) {
      // maybe load word embeddings
      val wordembfile = conf.rbgOptions().map(_.split(":")).filter(_.array(0) == "word-vector")
      if (wordembfile.size == 1)
        depParser.pipe.loadWordVectors(wordembfile.head(1))
    }
    in.close()
    depParser.pipe.closeAlphabets()
    lstmScorer = new LstmScorer(depParser.pipe, conf)
    if (! conf.trainLstmOnly())
      lstmScorer.loadModel(conf.modelFile())
  }


  object NewStatesRunner {
    val l = Logger(LoggerFactory.getLogger("NewStatesRunner"))
  }
  /**
    * Runnable that fills newStatesQueue with states from newStatesStream
    */
  class NewStatesRunner(newStatesStream: ()=> Stream[T],
    newStatesQueue: BlockingQueue[Option[T]]) extends Runnable {

    val performStatistics: Boolean = NewStatesRunner.l.underlying.isDebugEnabled
    var sumElementsInQueue = 0
    var sumPuts = 0

    override def run() = {
      if (performStatistics) {
        sumElementsInQueue = 0
        sumPuts = 0
      }
      // Thread.currentThread().setName("NewStatesRunner");
      NewStatesRunner.l.trace("starting to put somes")
      try{
        newStatesStream().foreach((x) => {
          newStatesQueue.put(Some(x))
          NewStatesRunner.l.trace(s"put ${x} into the queue")
          if (performStatistics) {
            sumElementsInQueue += newStatesQueue.size()
            sumPuts += 1
          }
        })
      } catch { case e: Throwable => {logger.error(e.toString); System.exit(1)}}
      NewStatesRunner.l.trace("starting to put nones")
        (1 to _numThreads).foreach((x)=>newStatesQueue.put(None))
      if (performStatistics)
        NewStatesRunner.l.debug("Average number of Elements in queue: " +
          (sumElementsInQueue / sumPuts).toString +
          "; number of states: " + sumPuts)
    }
  }


  def generateNewStateStream(previousStates: Iterable[T], newWord: String,
    newPos: String, newCPos: String, newLemma: String): Stream[T]

  /**
   * Hook that is called before the TrainRunners run, use this to setup whatever you want
   */
  def setupBeforeTrainRunners() = {}

  val evaluateOutput = ! conf.testSkipEvaluation()

  /**
    * Core method that performs training on a single instance, given a
    * set of results for the previous increment
    */
  def processInstance(previousStates: Iterable[T], goldInstance: DependencyInstance, train: Boolean, prefixLength: Int):
  (Boolean, Iterable[TrainStruct[T]]) = {

    var modelUpdated = false
    _trainIter += 1
    // General value definitions
    val newWordIndex = prefixLength
    val newWord = goldInstance.forms(newWordIndex)
    val newPos = goldInstance.postags(newWordIndex)
    val newCPos = goldInstance.cpostags(newWordIndex)
    val newLemma = if (goldInstance.lemmas != null)
      goldInstance.lemmas(newWordIndex)
    else
      null

    logger.debug("processing \n" +
         goldInstance.forms.mkString("/") +
         "\n with heads " +
         goldInstance.heads.mkString("/")
    )

    // define the stream of possible new states (note "def" so we
    // don't have a var preventing the stream from being gc-ed!
    val newStates : () => Stream[T] = () => generateNewStateStream(previousStates, newWord, newPos, newCPos, newLemma)

    setupBeforeTrainRunners()

    singleThreadSW.stop()
    multiThreadSW.start()

    // run the State generator in its own Threadpool
    var outQueue: BlockingQueue[Option[T]] = new ArrayBlockingQueue[Option[T]](_numThreads * 100)

    newStatesPool.submit(new NewStatesRunner(newStates, outQueue))

    val augmentLoss = conf.augmentLoss() && train

    // construct all runners and connect them by queues
    if (conf.useStandardScoring()) {
      val newOutQueue = new LinkedBlockingQueue[Option[T]]()
      (1 to _numThreads).foreach(_ => rbgScoringThreadPool.submit(new RBGScorer[T](this, outQueue, newOutQueue)))
      outQueue = newOutQueue
    }
    if (conf.lstmSubcommand.useTreeScorer() || conf.lstmSubcommand.useFirstOrderScorer()) {
      val newOutQueue = new LinkedBlockingQueue[Option[T]]()
      lstmScoringThreadPool.submit(new LstmScoringRunner[T](lstmScorer, outQueue, newOutQueue, _numThreads, depParser))
      outQueue = newOutQueue
    }
    val errorComputed = new LinkedBlockingQueue[Option[TrainStruct[T]]]()
    if (evaluateOutput)
      (1 to _numThreads).foreach(_=>errorComputerThreadPool.submit(new ErrorComputer[T](goldInstance, this, outQueue, errorComputed, 1)))
    else errorComputerThreadPool.submit(new Runnable {
      override def run(): Unit = {
        var numNonesConsumed = 0
        while (numNonesConsumed < _numThreads) {
          outQueue.take() match {
            case None => {numNonesConsumed += 1; errorComputed.put(None)}
            case Some(state) => errorComputed.put(Some(new TrainStruct[T](state)))
          }
        }
        }
      })

    var numNonesConsumed = 0
    var mostViolatingScore = -999999.9
    var mostViolatingStruct: TrainStruct[T] = null

    var leastNumErrorsWeighted = 999999.0
    var leastErrorsStructs = new ArrayBuffer[TrainStruct[T]]()
    val ordering: Ordering[TrainStruct[T]] = Ordering.by (elem => -elem.score)
    val bestScoringStructs = new mutable.PriorityQueue[TrainStruct[T]]()(ordering)
    var worstScoreStillInBeam = -99999999.9

    while (numNonesConsumed < _numThreads) {
      errorComputed.take() match {
        case Some(struct) =>
          val errorsUnlabeledWeighted = struct.numErrors.unlabeledWeighted
          // 1. check whether we have to update the most violating structure
          if (evaluateOutput) {
            val scoreAndErrors = struct.score + errorsUnlabeledWeighted
            if (scoreAndErrors > mostViolatingScore) {
              mostViolatingScore = scoreAndErrors
              mostViolatingStruct = struct
            }
            // check for least errors
            if (errorsUnlabeledWeighted < leastNumErrorsWeighted) {
              leastNumErrorsWeighted = errorsUnlabeledWeighted
              leastErrorsStructs.clear()
              leastErrorsStructs.append(struct)
            } else if (errorsUnlabeledWeighted == leastNumErrorsWeighted) {
              leastErrorsStructs.append(struct)
            }
          }

          // check whether to add to beam
          if (struct.score > worstScoreStillInBeam) {
            bestScoringStructs += struct
            if (bestScoringStructs.size > beamSize) {
              bestScoringStructs.dequeue()
              worstScoreStillInBeam = bestScoringStructs.head.score
            }
          }
        case None => numNonesConsumed += 1
      }
    }

    val bestScoringStruct = bestScoringStructs.maxBy(_.score)
    logger.trace(s"${leastErrorsStructs.length} least errors trees")
    // val bestScoringStruct = trainStructs.peek()
    val leastErrorsStruct = if (train) {
      leastErrorsStructs.minBy((x) => {
        computeErrorsWithMatch(x.state.getInstance, mostViolatingStruct.state.getInstance).unlabeledWeighted
      })
    } else if (evaluateOutput) {
      // doesn't matter during testing, as it is just used to count search errors
      leastErrorsStructs(0)
    } else {
      bestScoringStruct
    }
    logger.trace("least errors num errors unlabeled: " + leastNumErrorsWeighted)
    if (lastLeastErrorsWeighted < leastNumErrorsWeighted) {
      transitionErrorLogger.info("no correct transition found! gold:\n" +
        leastErrorsStruct.state.toString + "\n" +
        goldInstance.heads.mkString("/") + "\n" +
        goldInstance.postags.mkString("/") + "\n" +
        goldInstance.forms.mkString(" ") + "\n")
    }
    lastLeastErrorsWeighted = leastNumErrorsWeighted

    singleThreadSW.start()
    multiThreadSW.stop()
    if (bestScoringStructs.isEmpty) {
      logger.error("trainStructs is empty!")
      logger.error("previous states:" + previousStates)
    }
    // assert(trainStructs.size() > 0, "trainStructs is empty!")
    labelInstance(bestScoringStruct.state)
    if (bestScoringStruct != leastErrorsStruct)
      labelInstance(leastErrorsStruct.state, train = train)
    val bestScoringInstance = bestScoringStruct.state.getInstance

    if (conf.htmlErrorLog())
      errorLogger.logIncrement(bestScoringStruct, leastErrorsStruct, goldInstance,
        bestMatch(goldInstance.heads, bestScoringStruct.state.getHeads, bestScoringStruct.state.lastWord),
        bestMatch(goldInstance.heads, leastErrorsStruct.state.getHeads, leastErrorsStruct.state.lastWord))

    if (train) {
      // since we computed the score for each state, each state already has an instance
      val fearStruct = if (conf.augmentLoss()) mostViolatingStruct else bestScoringStruct
      val fear = if (conf.augmentLoss()) mostViolatingStruct.state.getInstance else bestScoringInstance
      val fearScore = if (conf.augmentLoss()) mostViolatingScore else bestScoringStruct.score

      logger.debug("Errors: " + bestScoringStruct.numErrors + " score: " + bestScoringStruct.score)
      val gold = leastErrorsStruct.state.getInstance

      val fearlfd = new IncLocalFeatureData(fear, depParser, false, _fvCache, _scoreCache, mostViolatingStruct.state.lastWord)
      val feargfd = new parser.IncGlobalFeatureData(fearlfd)
      val lfdTrain = new IncLocalFeatureData(gold, depParser, false, _fvCache, _scoreCache, mostViolatingStruct.state.lastWord)
      val gfdTrain = new parser.IncGlobalFeatureData(lfdTrain)

      // train structure
      // see what we potentially update
      val dynetModelInuse = conf.lstmSubcommand.useFirstOrderScorer() || conf.lstmSubcommand.useTreeScorer()
      val trainStandardModel = conf.useStandardScoring() && ! conf.trainLstmOnly()
      val updateRBGModel = trainStandardModel && (_trainIter %2 == 0 || ! dynetModelInuse)
      val updateDynetModel = dynetModelInuse && (_trainIter % 2 == 1 || ! trainStandardModel)

      // distance from predicted to least error score
      val numErrorsToBest = computeErrorsWithMatch(fear, leastErrorsStruct.state.getInstance).unlabeledWeighted

      if (updateDynetModel) {
        lstmScorer.train(mostViolatingStruct.state.getInstance, mostViolatingStruct.state.getScore.toFloat,
          bestScoringInstance, bestScoringStruct.state.getScore.toFloat,
          gold, leastErrorsStruct.state.getScore.toFloat,
          numErrorsToBest.toFloat)
      }

      if (fearStruct.numErrors.unlabeledWeighted > leastNumErrorsWeighted)
      {
        modelUpdated = true

        if (updateRBGModel)
         {
          val goldLocalFv = lfdTrain.getFeatureVector(gold)
          val goldGlobalFv = gfdTrain.getFeatureVector(gold)

          goldLocalFv.addEntries(fearlfd.getFeatureVector(fear), -1.0)
          goldGlobalFv.addEntries(feargfd.getFeatureVector(fear), -1.0)
          goldGlobalFv.addEntries(goldLocalFv)
          val loss = depParser.parameters.updateWithFv(_trainIter, numErrorsToBest, goldGlobalFv)
          logger.debug("RBG scorer training loss: " + loss)
        }
      } else {
        logger.trace("Dance! best tree is correct!")
      }
      // train labeling if we made labeling errors in addition to structure errors
      if (trainStandardModel && leastErrorsStruct.numErrors.labeled > leastErrorsStruct.numErrors.unlabeled) {
        // Constructor creates a shallow copy, duplicate everything we will change
        val bestWithGoldLabel = new DependencyInstance(leastErrorsStruct.state.getInstance)
        bestWithGoldLabel.deprels = bestWithGoldLabel.deprels.clone()
        bestWithGoldLabel.deplbids = bestWithGoldLabel.deplbids.clone()
        labelFromGold(bestWithGoldLabel, goldInstance, bestMatch(goldInstance.heads, bestWithGoldLabel.heads, leastErrorsStruct.state.lastWord))
        depParser.parameters.updateLabel(bestWithGoldLabel, leastErrorsStruct.state.getInstance, lfdTrain, gfdTrain, _trainIter, 0)
      }
    }

    if (evaluateOutput) {
      errorStats.addToStats(bestScoringStruct, goldInstance,
        bestScoringStruct.state.lastWord,
        bestMatch(goldInstance.heads, bestScoringInstance.heads, bestScoringInstance.lastNormalWord))
      bestErrorStats.addToStats(leastErrorsStruct, goldInstance,
        mostViolatingStruct.state.lastWord,
        bestMatch(goldInstance.heads, leastErrorsStruct.state.getHeads, leastErrorsStruct.state.lastWord))
      logger.debug(s"best number of errors: ${leastErrorsStruct.numErrors}")
    }
    // ********************* BEGIN ERROR STAT REPORTING *********************
    if (_trainIter % 1000 == 999) {
      println(s"after ${_trainIter} increments:")
      if  (evaluateOutput) {
        println("Predicted accuracy:")
        errorStats.printStats()
        println("Best in beam accuracy:")
        bestErrorStats.printStats()
      }
      logger.info(s"Time in singlethread: ${singleThreadSW} in multithread: ${multiThreadSW}, per increment: " +
        (multiThreadSW.elapsed(TimeUnit.MILLISECONDS) + singleThreadSW.elapsed(TimeUnit.MILLISECONDS))/_trainIter)
    }
    if (train && _trainIter % 2000 == 1499) {
      // reset counters to reflect current state in training
      errorStats = new ErrorStatsCollector[T](errorStats.frontierSize)
      bestErrorStats = new ErrorStatsCollector[T](errorStats.frontierSize)
    }
    // ********************* END ERROR STAT REPORTING *********************

    // val bestScoringStructs: ArrayBuffer[TrainStruct[T]] = new ArrayBuffer[TrainStruct[T]]()
    if (train || conf.goldAlwaysInTest()) {
      // assume that we picked the best state before to prevent repeated updates in later
      // increments which have already been learned above.
      bestScoringStructs += leastErrorsStruct
    }
    // bestScoringStructs.appendAll(trainStructs.asScala)
    return (modelUpdated, bestScoringStructs)
  }



  /**
    * Computes the score of state according to the current model
    */
  def computeScore(state: BaseState[T]): Double = {
    try {
      val score = if (!conf.useStandardScoring()) {
        0.0
      } else {
        val inst = state.toInstance(depParser)
        val compute_f = (_: String) => {
          val lfd = new IncLocalFeatureData(inst, depParser, false, _fvCache, _scoreCache, state.lastWord)
          // TODO only pass lfd
          Tuple2(lfd, lfd.gfd)
        }
        val fd = _featureCache.get(state.fdInformation, compute_f.asJava)

        val lfd = fd._1
        val gfd = fd._2
        val arcList = new DependencyArcList(inst.heads, options.useHO)

        lfd.getScore(inst, arcList) + gfd.getScore(inst, arcList)
      }
      score
    } catch {
      case e: Throwable => {logger.error(s"Couldn't compute score for ${state} \n with heads ${state.getInstance.heads.mkString(",")}"); throw(e)}
    }
  }

  def labelInstance(state: BaseState[T], train: Boolean = false): Unit = {
    val inst = state.toInstance(depParser)
    val compute_f = (_: String) => {
        val lfd = new IncLocalFeatureData(inst, depParser, false, _fvCache, _scoreCache, state.lastWord)
        Tuple2(lfd, new parser.IncGlobalFeatureData(lfd))
      }
    val fd = _featureCache.get(state.fdInformation, compute_f.asJava)
    val lfd = fd._1
    // this changes inst.deplbids in place
    lfd.predictLabels(state.getHeads, inst.deplbids, train)
    var i = 1
    while (i < state.getHeads.size) {
      val label = depParser.pipe.types(inst.deplbids(i))
      inst.deprels(i) = label
      state.getDeprel(i) = label
      i += 1
    }
  }

  def labelFromGold(inst: DependencyInstance, gold: DependencyInstance, matching: Seq[Int]): Unit = {
    val sheads = inst.heads
    val gheads = gold.heads
    for (i <- 1 until inst.forms.length) {
      if (matching(i) >= 0 && matching(sheads(i)) == gold.heads(matching(i))) {
        inst.deprels(i) = gold.deprels(matching(i))
        inst.deplbids(i) = gold.deplbids(matching(i))
      }
    }
  }

  /**
    * Sorts states and returns the #beamsize best ones
    */
//  def clipBeam(states: collection.Seq[T]): Seq[T] = {
//    states.sortBy((x) => computeScore(x) * -1).slice(0, beamSize)
//  }

  /**
    * Performs a single parsing step (backtrack, extend word, create states, clip to #beamsize)
    */
  def doStep(states: collection.Seq[T], word: String, pos:String, lemma: String): Seq[T] ;

  def getEmptyState(): T;

  /**
    * parses a sentence incrementally
    */
  def parse(words: List[String], posList: List[String], lemmas: List[String]): DependencyInstance = {
    var currStates: Seq[T] = List(getEmptyState())
    var result: DependencyInstance = null
    (0 until words.size) foreach ((i) => {
      val word = words(i)
      val pos = posList(i)
      logger.debug("\n====================================\n" + word + "\t" + pos)
      val t0 = System.nanoTime()
      currStates = doStep(currStates, word, pos, lemmas(i))
      val t1 = System.nanoTime()
      currStates.foreach((c) => logger.debug(c.toString))
      logger.debug(s"Time: %d ${(t1-t0)/1000/1000} ms\n")
      result = currStates(0).toInstance(depParser)
      //Console.println(currStates(0))
    })
    result
  }


  /**
    * Computes attachment errors by matching state to gold
    */
  def computeErrorsWithMatch(predicted: DependencyInstance, goldInstance: DependencyInstance): AttachmentErrors = {
    val bmatch = bestMatch(goldInstance.heads, predicted.heads, predicted.lastNormalWord)
    ErrorStatsCollector.computeErrorsWithMatch(predicted, goldInstance, lastNormalWord(predicted.forms), bmatch)
  }
}
