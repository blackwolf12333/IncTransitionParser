package utils

import gnu.trove.map.hash.TIntIntHashMap

import scala.util.Random

/**
  * implementation of frequency based word dropout (Iyyer et al. 2015)
  * By default has the same alpha as kiperwasser and Goldberg (2016)
  *
  * Does not need to be serialized as it is only used during training.
  * @param alpha defaults to 0.2
  */
@SerialVersionUID(45L)
class FrequencyCounter(alpha: Float=0.2f) extends Serializable {
  val frequencyDict = new TIntIntHashMap()
  def count(idx: Int): Unit = {
    frequencyDict.adjustOrPutValue(idx, 1, 1)
  }

  /**
    * return true with prob alpha / (count idx + alpha)
    * @param idx
    * @return
    */
  def shouldDropOut(idx: Int): Boolean = {
    alpha / (frequencyDict.get(idx) + alpha) > Random.nextFloat()
  }

}
