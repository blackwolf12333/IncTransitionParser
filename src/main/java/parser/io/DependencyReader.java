package parser.io;


import java.io.*;
import java.util.zip.GZIPInputStream;

import parser.DependencyInstance;
import parser.Options;

public abstract class DependencyReader {
	
	BufferedReader reader;
	boolean isLabeled;
	
	public static DependencyReader createDependencyReader(Options options) {
		String format = options.format;
		if (format.equalsIgnoreCase("CONLL06") || format.equalsIgnoreCase("CONLL-06")) {
			return new Conll06Reader();
		} else if (format.equalsIgnoreCase("CONLLX") || format.equalsIgnoreCase("CONLL-X")) {
			return new Conll06Reader();
		} else if (format.equalsIgnoreCase("CONLLU") || format.equalsIgnoreCase("CONLL-U")) {
			return new Conll06Reader();
		} else if (format.equalsIgnoreCase("CONLL09") || format.equalsIgnoreCase("CONLL-09")) {
			return new Conll09Reader();
		} else {
			System.out.printf("!!!!! Unsupported file format: %s%n", format);
			return new Conll06Reader();
		}
	}
	
	public abstract DependencyInstance nextInstance() throws IOException;
	public abstract boolean IsLabeledDependencyFile(String file) throws IOException;
	
	public boolean startReading(String file) throws IOException {
		isLabeled = IsLabeledDependencyFile(file);
		InputStream fstream = new FileInputStream(file);
		if (file.endsWith(".gz"))
			fstream = new GZIPInputStream(fstream);
		reader = new BufferedReader(new InputStreamReader(fstream, "UTF8"));
		return isLabeled;
	}
	
	public void close() throws IOException { if (reader != null) reader.close(); }

    
}
