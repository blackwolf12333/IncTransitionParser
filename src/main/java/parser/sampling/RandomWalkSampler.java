package parser.sampling;

import java.util.*;
import java.util.stream.IntStream;

import gnu.trove.list.array.*;
import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import parser.*;
import utils.Utils;

public class RandomWalkSampler {
	
	Random r;
	Options options;
	final int labelLossType;
	public THashMap<String, THashSet<String>> possibleEdgesBetweenPOS;

	public RandomWalkSampler(int seed, Options options) {
		r = new Random(seed);
		this.options = options;
		labelLossType = options.labelLossType;
	}
	
	public RandomWalkSampler(Options options) {
		r = new Random(1/*System.currentTimeMillis()*/);
		this.options = options;
		labelLossType = options.labelLossType;
	}
	
	public RandomWalkSampler(Random r, Options options) {
		this.r = r;
		this.options = options;
		labelLossType = options.labelLossType;
	}

	public void setPossibleEdgesBetweenPOS(THashMap<String, THashSet<String>> possibleEdgesBetweenPOS) {
		this.possibleEdgesBetweenPOS = possibleEdgesBetweenPOS;
	}
	
    public DependencyInstance uniformRandomWalkSampling(DependencyInstance inst,
    		LocalFeatureData lfd, boolean addLoss)
    {
        //int cnt = 0;
    	int len = inst.length;
    	
		DependencyInstance predInst = new DependencyInstance(inst);
		predInst.heads = new int[len];
		predInst.deplbids = new int[len];
        
        //double[] score = new double[len];
        //int[] depList = new int[len];
        //int size = 0;

    	boolean[] inTree = new boolean[len];
    	inTree[0] = true;
    	for (int i = 0; i < len; i++) {
    		predInst.heads[i] = -1;
    	}

    	// we want to create a single tree, but the
		// punctuation may be attached to 0 all the time.
		// Track if we already constructed a path to 0 to forbid that later on
    	boolean alreadyAttachedNonPunctToRoot = false;
    	for (int i = 1; i < len; i++) {
    		int curr = i;
    		TIntArrayList currentBranch = new TIntArrayList();
    		currentBranch.add(i);
    		while (!inTree[curr]) {
    			int headCandidate;
    			if (possibleEdgesBetweenPOS != null) {
    				// sample from all heads with fitting POS tag
					THashSet<String> pHeads = possibleEdgesBetweenPOS.get(predInst.postags[curr]);
					// if this word can be attached to a prediction but there is no prediction,
					// we need to prevent the fallback of attaching anywhere.
					// In this case, null is returned to only produce structures with prediction
					boolean attachmentToPredictionPossible = pHeads != null && pHeads.contains("VVIRT");
					int[] possibleHeads = {};
					if (pHeads != null) {
						int startIndex = 1;
						if (!alreadyAttachedNonPunctToRoot || pHeads.size() == 1) {
							// evil hack to find puncuation always attached to 0
							// if something else has this property it's no problem since the filtering below
							// will simply filter out 0 based on POS
							startIndex = 0;
						}
						possibleHeads = IntStream.range(startIndex, len)
								// prevent possible cycles
								.filter((int x) -> !currentBranch.contains(x))
								// only attach to words licensed by matching POS tag
								.filter((int x) -> pHeads.contains(predInst.postags[x]))
								.toArray();
					}
					// pHeads was null or our filters removed all elements
    				if (possibleHeads.length == 0) {
    					if (attachmentToPredictionPossible && inst.lastNormalWord == inst.length-1)
    						return null;
    					else
							possibleHeads = IntStream.range(0, len).toArray();
					}
					headCandidate = possibleHeads[r.nextInt(possibleHeads.length)];
    				// we reached 0 and did not try to attach punctuation (pHeads size equals 1)
    				if (headCandidate == 0 && ! (pHeads != null && pHeads.size() == 1))
    					alreadyAttachedNonPunctToRoot = true;
				} else {
    				// traditional approach
					int st = lfd.startIndex(curr);
					int size = lfd.endIndex(curr) - st;
					int sample = r.nextInt(size);
					headCandidate = lfd.getHead(st + sample);
				}
    			predInst.heads[curr] = headCandidate;
    			curr = headCandidate;
    			currentBranch.add(curr);
    			//predInst.heads[curr] = depList[sample];
    			// curr = predInst.heads[curr];
    		}
    		curr = i;
    		while (!inTree[curr]) {
    			inTree[curr] = true;
    			curr = predInst.heads[curr]; 
    		}
    	}
    	
    	return predInst;
    }
    
    public DependencyInstance randomWalkSampling(DependencyInstance inst,
    		LocalFeatureData lfd, boolean addLoss)
    {
        //int cnt = 0;
    	int len = inst.length;
    	
		DependencyInstance predInst = new DependencyInstance(inst);
		predInst.heads = new int[len];
		predInst.deplbids = new int[len];
        
        double[] score = new double[len];
        int[] depList = new int[len];
        int size = 0;

    	boolean[] inTree = new boolean[len];
    	inTree[0] = true;
    	for (int i = 0; i < len; i++) {
    		predInst.heads[i] = -1;
    	}
    	
    	for (int i = 1; i < len; i++) {
    		int curr = i;
    		while (!inTree[curr]) {
    			// sample new head 
                size = 0;

    			for (int candH = 0; candH < len; candH++) {
    				if (candH == curr || lfd.isPruned(candH, curr))
    					continue;
    				
    				double s = lfd.getArcScore(candH, curr);
    				
    				if (addLoss) {
    					  if (candH != inst.heads[curr])
    						s += 1.0;
    				}
                    score[size] = s;
                    depList[size] = candH;
                    ++size;
    			}

    			int sample = samplePoint(score, size, r);
    			predInst.heads[curr] = depList[sample];
    			curr = predInst.heads[curr];
    		}
    		curr = i;
    		while (!inTree[curr]) {
    			inTree[curr] = true;
    			curr = predInst.heads[curr]; 
    		}
    	}
    	
    	return predInst;
    }
    
    private int samplePoint(double[] score, int N, Random r) {
    	double sumScore = Double.NEGATIVE_INFINITY;
    	for (int i = 0; i < N; i++) {
    		sumScore = Utils.logSumExp(sumScore, score[i]);
    	}
    	double logp = Math.log(r.nextDouble() + 1e-60);
    	double cur = Double.NEGATIVE_INFINITY;
    	int ret = 0;
    	for (; ret < N; ret++) {
    		cur = Utils.logSumExp(cur, score[ret]);
    		if (logp + sumScore < cur)
    			break;
    	}
    	return ret;
    }
    
//    private void dumpScoreTable(int len, DependencyInstance inst,
//			LocalFeatureData lfd, int[][] staticTypes, boolean addLoss) {
//    	System.out.println(len);
//        for (int u = 0; u < len; ++u) {
//            for (int v = 0; v < len; ++v) 
//                if (v == 0 || u == v || lfd.isPruned(u, v))
//                    System.out.print("0.00\t");
//                else {
//                    double s = lfd.getArcScore(u, v);
//                    //int l = options.learnLabel ? staticTypes[u][v] : 0;
//                    //s += options.learnLabel ? lfd.getLabeledArcScore(u, v, l) : 0.0;
//                    if (addLoss) {
//                        s += (inst.heads[v] != u ? 1.0 : 0.0);
//                        //   + (options.learnLabel && inst.deplbids[v] != l ? 1.0 : 0.0);
//                    }
//                    System.out.printf("%.2f\t", s);
//                }
//            System.out.println();
//        }
//        System.exit(1);		
//	}

}
