package parser.scoring

import java.util
import java.util.concurrent.{BlockingQueue, TimeUnit}

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import parser.{AbstractTransitionParser, DependencyInstance, ErrorStatsCollector, TrainStruct}
import parser.decoding.BaseState
import parser.evaluation.Matching

object ErrorComputer {
  val logger = Logger(LoggerFactory.getLogger(this.getClass))
}

class ErrorComputer[T<: BaseState[T]] (goldInstance: DependencyInstance,
                                       tp: AbstractTransitionParser[T],
                                       in: BlockingQueue[Option[T]],
                                       out: BlockingQueue[Option[TrainStruct[T]]],
                                       numNoneToConsume: Int) extends Runnable  {
  import ErrorComputer.logger
  private var numNonesConsumed = 0
  override def run(): Unit = {
    logger.trace("starting...")
    while (numNonesConsumed < numNoneToConsume) {
      in.take() match {
        case Some(currState) => {
          logger.trace("consuming some")
          val bmatch = Matching.bestMatch(goldInstance.heads, currState.getHeads, currState.lastWord)
          val errors = ErrorStatsCollector.computeErrorsWithMatch(currState.toInstance(tp.depParser),
            goldInstance, currState.lastWord, bmatch)
          val struct = new TrainStruct[T](currState)
          struct.numErrors = errors
          out.put(Some(struct))
        }
        case None => {
          logger.trace("consuming none")
          numNonesConsumed += 1
        }
      }
    }
    for (i <- 0 until numNoneToConsume)
      out.add(None)
  }
}
