package parser

import parser.decoding.BaseState
import scala.language.implicitConversions

/*
 * unlabeled: number of attachments to the wrong word
 * labeled: unlabeled + attachments with wrong label
 * predicted: attachments to VN1 if gold is attached to VN2 (a subset of unlabeled errors)
 */
sealed case class AttachmentErrors(uprefix: Int, lprefix: Int, upredicted: Int, lpredicted: Int, notpredicted: Int) {
  def labeled = lpredicted + lprefix + notpredicted
  def unlabeled = upredicted + uprefix + notpredicted
  def unlabeledWeighted = 0.5*upredicted + 3*uprefix + 0.1*notpredicted
  def +(other: AttachmentErrors) = AttachmentErrors(
    this.uprefix+other.uprefix,
    this.lprefix+other.uprefix,
    this.upredicted + other.upredicted,
    this.lpredicted + other.lpredicted,
    this.notpredicted + other.notpredicted)
}

object EmptyErrors extends AttachmentErrors(0,0,0,0,0)


class TrainStruct[T<:BaseState[T]](val state: T) {
  def score = state.getScore
  var numErrors: AttachmentErrors = EmptyErrors
  // var loss: Double = -1

  override def equals(o: scala.Any): Boolean = {
    o match {
      case o: TrainStruct[T] => o.state == state;
      case _ => false;
    }
  }
}
